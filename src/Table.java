import java.util.List;

public interface Table {
    List<Table> getExportTable();
    List<Table> getExportLoadTable();
}
