import java.util.List;

public interface Lane {

    // adding and removing

    int addVehicleToLane ( Vehicle vehicle, boolean direction) throws Exception;

    int removeVehicleFromLane();

    // sort and calculation
    void sortLanes ();

    void calculateAvailableLength ();

    void calculateInitialSetPoint ();


    // set functions

    void setPriority(int priority);

    // get functions

    String getName();

    String getDeck();

    float getCenterX();

    float getCenterZ();

    float getCenterY();

    float getMinX();

    float getMaxX();

    Lane getBestLane ();

    float getAvailableLength ();

    float getLaneMass ();

    float getPriority();

    int getDefaultPriority();

    float getLaneCoefficient();

    float getLaneCoefficientReversed();

    void getLoadedVehiclesOnLane();

    // printing data

    void printLaneMass(Lane lane);

    // returning List

    List<VehicleType> getAllowedVehicleTypes();

    List<VehicleType> getNotAllowedVehicleTypes();

    List<Vehicle> getVehicleList();

    void exportToTable(List<Vehicle> vehicleList, Lane lane, List<Record> outputTable);

    void setCenterX(float a, float b);
}
