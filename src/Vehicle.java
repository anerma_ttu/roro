public interface Vehicle {
    String getRegistrationNumber ();
    void setRegistrationNumber (String registrationNumber);
    VehicleType getVehicleType();
    float getVehicleLength ();
    float getVehicleWidth ();
    float getVehicleHeight();
    float getxMin();
    float getxMax();
    void  setXmax(float v);
    void  setXmin(float v);
    void getVehicleData ();
    float getVehicleMass();
    String getLaneString ();
    String getDeckString ();
}
