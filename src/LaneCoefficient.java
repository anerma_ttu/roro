public enum LaneCoefficient {
    P1 (100000f),
    P2 (1000f),
    P3 (10f),
    P4 (1f),
    P5 (0.1f),
    P6 (0.01f);

    private float laneCoefficient;

    LaneCoefficient(float laneCoefficient)
    {
        this.laneCoefficient=laneCoefficient;
    }

    float getLaneCoefficient ()
    {
        return laneCoefficient;
    }
}
